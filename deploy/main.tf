/*
Recommended Practice:  
  Run terraform within docker-compose so that
   - you're not locked to a specifi terraform version running on your local machine
   - there might be different versions of terraform on your local machine
   - if you run tf with a new version, it upgrades your state file to the new version (& its not backward compatible)
   - with docker-compose, you can specify which version to use  
*/

terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-jjterraform"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

/*This will be used to retrive the current region we are working on*/
data "aws_region" "current" {}

